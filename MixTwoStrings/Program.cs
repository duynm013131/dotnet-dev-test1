﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        // xem file README.md
        var common = string.Concat(value1.Zip(value2, (a, b) => new[] { a, b }).SelectMany(c => c));
        var shortestLength = Math.Min(value1.Length, value2.Length);
        var result = common + value1.Substring(shortestLength) + value2.Substring(shortestLength);
        return result;
        //throw new NotImplementedException();
    }
}

