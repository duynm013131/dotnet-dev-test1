﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        for (int i = 0; i < x.Length; i++)
        {
            for (int j = i + 1; j < x.Length; j++)
            {
                if (x[i] < x[j])
                {

                    int temp = x[i];
                    x[i] = x[j];
                    x[j] = temp;
                }
            }
        }

        for (int i = 0; i < x.Length; i++)
        {
            Console.Write(x[i] + " ");
        }
        return x;
        throw new NotImplementedException();
    }

}