﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        int result = 0;

        int largest = int.MinValue;
        int second = int.MinValue;
        if (x.Length == 1 || x.Length == 0)
        {
            result = -1;
        }
        else
        {
            foreach (int i in x)
            {
                if (i > largest)
                {
                    second = largest;
                    largest = i;
                }
                else if (i > second)
                    second = i;
            }

            for (int y = 0; y < x.Length; y++)
            {
                if (x[y] == second)
                {
                    result = y; break;
                }
            }
        }
        return result;
        //throw new NotImplementedException();*/
    }
}